<?php
/**
 * Created by PhpStorm.
 * User: houlder
 * Date: 30/01/19
 * Time: 17:04
 */

namespace App\Controller;


use App\Modele\ExampleModele;
use Root\BaseController;
use Root\MyContainer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends BaseController
{
    public function index(Request $request)
    {
        //var_dump($request->getPathInfo()); die;
        return new Response("Laza houlder");
    }
}