<?php
/**
 * Created by PhpStorm.
 * User: houlder
 * Date: 03/02/19
 * Time: 00:46
 */

namespace App\Modele;


use Doctrine\DBAL\Connection;

class ExampleModele
{
    /**
     * @var Connection
     */
    private $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }
}