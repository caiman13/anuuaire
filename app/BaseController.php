<?php
/**
 * Created by PhpStorm.
 * User: houlder
 * Date: 30/01/19
 * Time: 17:04
 */

namespace Root;



use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class BaseController
{
    /**
     * @var MyContainer
     */
    private $container;

    /**
     * BaseController constructor.
     * @param MyContainer $container
     */
    public function __construct(MyContainer $container)
    {
        $this->container = $container;
    }

    /**
     * Function to get service from container
     * @param $name
     * @return mixed
     */
    public function get($name)
    {
        return $this->container[$name];
    }

    /**
     * Function to render a template
     * @param $templateName
     * @param array $params
     * @return Response
     */
    public function render($templateName, array $params = [])
    {
        $contents = $this->container->offsetGet("twig")->render($templateName, $params);

        return new Response($contents);
    }

    /**
     * Redirect to url
     * @param $url
     * @param int $status
     * @return RedirectResponse
     */
    public function redirect($url, $status = 302)
    {
        return new RedirectResponse($url, $status);
    }

    /**
     * Redirect to route
     * @param $route
     * @param array $parameters
     * @param int $status
     * @return RedirectResponse
     */
    public function redirectToRoute($route, array $parameters = array(),$status = 302)
    {
        return $this->redirect($this->container["url_generator"]->generate($route,$parameters), $status);
    }
}