<?php
/**
 * Created by PhpStorm.
 * User: houlder
 * Date: 30/01/19
 * Time: 10:25
 */

namespace Root;


use Pimple\Container;
use Root\Provider\DoctrineServiceProvider;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Yaml\Yaml;

class MyContainer extends Container
{
    /**
     * @var string
     */
    private $folder;

    /**
     * MyContainer constructor.
     * @param array $values
     */
    public function __construct(array $values = array())
    {
        parent::__construct($values);

        $this->folder = implode(DIRECTORY_SEPARATOR, array(__DIR__, '..', 'conf', ''));


        $this->__init();
    }

    /**
     * @param $name
     * @return mixed
     */
    protected function __readConfig($name)
    {
        return Yaml::parseFile($this->folder . $name);
    }

    protected function __databaseParams()
    {
        $this["db.options"] = $this->__readConfig("database.yaml");

        return $this;
    }

    protected function __fillParams()
    {
        $this["routes"] = function ($c) {
            $folder = implode(DIRECTORY_SEPARATOR, array(__DIR__, '..', 'conf', ''));
            //var_dump(dir(__DIR__)); die;
            $fileLocator = new FileLocator($folder);
            $loader = new YamlFileLoader($fileLocator);
            $routes = $loader->load('routes.yaml');
            return $routes;
        };
        $parametters = $this->__readConfig("parametter.yaml");
        foreach ($parametters as $name=>$item){
            $this[$name] = $item;
        }
    }

    /**
     * @param $service
     * @param array $serviceConfig
     * @throws \ReflectionException
     */
    protected function __getService($service, array $serviceConfig)
    {
        $reflection = new \ReflectionClass($serviceConfig["class"]);

        $arguments = [];

        if (isset($serviceConfig["arguments"])) {
            foreach ($serviceConfig["arguments"] as $value) {
                $arguments[] = $this[$value];
            }
        }


        $this[$service] = function ($c) use ($reflection, $arguments) {

            return $reflection->newInstanceArgs($arguments);
        };

        if (isset($serviceConfig["alias"])) {
            $alias = $serviceConfig["alias"];

            $this[$alias] = $this[$service];
        }
    }

    protected function __getAllSerices()
    {
        $services = $this->__readConfig("service.yaml");

        foreach ($services as $name=>$item){
            $this->__getService($name, $item);
        }
    }

    protected function __getBaseService()
    {
        $services = $this->__readConfig("base_service.yaml");

        foreach ($services as $name=>$item){
            $this->__getService($name, $item);
        }
    }

    protected function __init()
    {
        $this->__fillParams();
        $this->__databaseParams();
        $this->register(new DoctrineServiceProvider());
        $this->__getBaseService();
        $this->__getAllSerices();
    }


}