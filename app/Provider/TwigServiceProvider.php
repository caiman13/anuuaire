<?php
/**
 * Created by PhpStorm.
 * User: houlder
 * Date: 02/02/19
 * Time: 20:39
 */

namespace Root\Provider;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Twig_Environment;
use Twig_Loader_Filesystem;

class TwigServiceProvider implements ServiceProviderInterface
{

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $container A container instance
     */
    public function register(Container $container)
    {
        $params = $container["twig"];


        $container["twig.template"] = function ($c) use ($params) {
            $loader = new Twig_Loader_Filesystem(__DIR__ . $c["templates"]);
            $twig = new Twig_Environment($loader, [
                'cache' => __DIR__ . $c["cache"],
            ]);

            if ($c['debug']) {
                $twig->addExtension(new \Twig_Extension_Debug());
            }

            return $twig;
        };
    }
}