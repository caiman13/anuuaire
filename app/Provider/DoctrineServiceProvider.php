<?php
/**
 * Created by PhpStorm.
 * User: houlder
 * Date: 02/02/19
 * Time: 23:01
 */

namespace Root\Provider;


use Doctrine\DBAL\DriverManager;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class DoctrineServiceProvider implements ServiceProviderInterface
{

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $container
     */
    public function register(Container $container)
    {
        $database = $container["db.options"];

        foreach ($database as $name=>$value){
            $id = "db." . $name;

            $container[$id] = function ($c) use ($value){
                $config = new \Doctrine\DBAL\Configuration();
                return DriverManager::getConnection($value, $config);
            };
        }
    }
}