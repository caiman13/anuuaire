<?php

namespace Root;

use JsonSchema\Exception\ResourceNotFoundException;
use Pimple\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\TerminableInterface;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Router;

class Application implements HttpKernelInterface , TerminableInterface
{
    /**
     * @var MyContainer
     */
    private $container;

    public function __construct(MyContainer $container)
    {
        $this->container = $container;
    }


    /**
     * Terminates a request/response cycle.
     *
     * Should be called after sending the response and before shutting down the kernel.
     * @param Request $request
     * @param Response $response
     */
    public function terminate(Request $request, Response $response)
    {
        // TODO: Implement terminate() method.
    }

    /**
     * Handles a Request to convert it to a Response.
     *
     * When $catch is true, the implementation must catch all exceptions
     * and do its best to convert them to a Response instance.
     *
     * @param Request $request A Request instance
     * @param int $type The type of the request
     *                         (one of HttpKernelInterface::MASTER_REQUEST or HttpKernelInterface::SUB_REQUEST)
     * @param bool $catch Whether to catch exceptions or not
     *
     * @return Response A Response instance
     *
     * @throws \Exception When an Exception occurs during processing
     */
    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {

        $matcher = $this->container["matcher"];

        $attributes = $matcher->matchRequest($request);

        $attributes['container'] = clone $this->container;

        $request->attributes->add($attributes);

        $controllerReslver = new ControllerResolver();

        $argumentResolver = new ArgumentResolver();

        $controller = $controllerReslver->getController($request);

        $arguments = $argumentResolver->getArguments($request, $controller);

        //var_dump($arguments); die;

        return call_user_func_array($controller,$arguments);
    }
}